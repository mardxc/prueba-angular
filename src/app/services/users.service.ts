import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import axios from 'axios';
import { Observable, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../interfaces/user.interfaces';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(private http: HttpClient) {}

  getAllUsers(): Observable<User[]> {
    return this.http.get<any>(environment.url + '/users');
  }
  createUser(obj: User): Observable<User[]> {
    return this.http.post<any>(environment.url + '/users', obj);
  }
  updateUser(obj: User): Observable<User[]> {
    return this.http.put<any>(environment.url + '/users', obj);
  }
  deleteUser(id: number): Observable<User[]> {
    return this.http.delete<any>(environment.url + '/users/' + id);
  }
  getUserById(id: number): Observable<User> {
    return this.http.get<any>(environment.url + '/users/' + id);
  }
}
