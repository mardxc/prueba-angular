import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { User } from 'src/app/interfaces/user.interfaces';
import { UsersService } from 'src/app/services/users.service';
import { CreateComponent } from '../create/create.component';
import { EditComponent } from '../edit/edit.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit {
  dataSource: User[] = [];
  displayedColumns: string[] = ['id', 'name', 'email', 'password', 'accion'];

  constructor(private usersService: UsersService, private dialog: MatDialog) {}
  ngOnInit(): void {
    this.getAllUsers();
  }
  getAllUsers() {
    const response = this.usersService.getAllUsers();
    response.subscribe({
      next: (data: User[]) => {
        this.dataSource = data;
      },
    });
  }
  deleteUser(id: number) {
    const response = this.usersService.deleteUser(id);
    response.subscribe({
      next: (data: User[]) => {
        this.getAllUsers();
      },
    });
  }
  nuevo() {
    const dialogRef = this.dialog.open(CreateComponent);

    dialogRef.afterClosed().subscribe((result) => {
      this.getAllUsers();
    });
  }
  editar(id: number) {
    const dialogRef = this.dialog.open(EditComponent, {
      data: {
        id: id,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.getAllUsers();
    });
  }
}
