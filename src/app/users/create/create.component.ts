import { Component } from '@angular/core';
import {
  Validators,
  AbstractControl,
  FormBuilder,
  FormGroup,
} from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { User } from 'src/app/interfaces/user.interfaces';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
})
export class CreateComponent {
  formNuevo: FormGroup = new FormGroup({});

  constructor(
    private fb: FormBuilder,
    private usersService: UsersService,
    public dialogRef: MatDialogRef<CreateComponent>
  ) {
    this.formNuevo = this.fb.group({
      email: [
        'juan@gmail.com',
        [
          Validators.required,
          Validators.email,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
        ],
      ],

      password: ['12345', [Validators.required]],
      name: ['juan', [Validators.required]],
    });
  }

  get form(): { [key: string]: AbstractControl } {
    return this.formNuevo.controls;
  }

  onSubmit() {
    if (!this.formNuevo.valid) {
      return;
    }
    let obj: User = {
      name: this.form['name'].value,
      email: this.form['email'].value,
      password: this.form['password'].value,
    };
    this.createUser(obj);
  }

  createUser(obj: User) {
    const response = this.usersService.createUser(obj);
    response.subscribe({
      next: (data: any) => {
        this.dialogRef.close(true);
      },
    });
  }
}
