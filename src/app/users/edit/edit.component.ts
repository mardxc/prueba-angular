import { Component, Inject, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from 'src/app/interfaces/user.interfaces';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css'],
})
export class EditComponent implements OnInit {
  formNuevo: FormGroup = new FormGroup({});
  id: number = 0;
  ngOnInit(): void {
    this.getUserById(this.id);
  }

  constructor(
    private usersService: UsersService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<EditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.id = data.id;
    this.formNuevo = this.fb.group({
      email: [
        '',
        [
          Validators.required,
          Validators.email,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
        ],
      ],

      password: ['', [Validators.required]],
      name: ['', [Validators.required]],
      id: ['', [Validators.required]],
    });
  }

  get form(): { [key: string]: AbstractControl } {
    return this.formNuevo.controls;
  }
  getUserById(id: number) {
    const response = this.usersService.getUserById(id);
    response.subscribe({
      next: (data: User) => {
        this.form['name'].setValue(data.name);
        this.form['email'].setValue(data.email);
        this.form['password'].setValue(data.password);
        this.form['id'].setValue(data.id);
      },
    });
  }

  onSubmit() {
    if (!this.formNuevo.valid) {
      return;
    }
    let obj: User = {
      id: this.form['id'].value,
      name: this.form['name'].value,
      email: this.form['email'].value,
      password: this.form['password'].value,
    };
    this.updateUser(obj);
  }

  updateUser(obj: User) {
    const response = this.usersService.updateUser(obj);
    response.subscribe({
      next: (data: any) => {
        this.dialogRef.close(true);
      },
    });
  }
}
