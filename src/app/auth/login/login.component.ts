import { Component } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/interfaces/user.interfaces';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  formLogin: FormGroup = new FormGroup({});

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.formLogin = this.fb.group({
      email: [
        'juan@gmail.com',
        [
          Validators.required,
          Validators.email,
          Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
        ],
      ],

      password: ['12345', [Validators.required]],
    });
  }

  get form(): { [key: string]: AbstractControl } {
    return this.formLogin.controls;
  }

  onSubmit() {
    if (!this.formLogin.valid) {
      return;
    }
    let obj: User = {
      email: this.form['email'].value,
      password: this.form['password'].value,
    };
    this.inicioSesion(obj);
  }

  inicioSesion(obj: User) {
    const response = this.authService.inicioSesion(obj);
    response.subscribe({
      next: (data: any) => {
        this.router.navigate(['users/home']);
      },
    });
  }
}
